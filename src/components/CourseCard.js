import React, {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}) {
const {name, description, price} = courseProp;
    
const [count, setCount] = useState(0);
const [seat, setSeat] = useState(30);
console.log(count);

const enroll = () => {
    setCount(count + 1);
    setSeat(seat - 1);
    console.log("Enrolees: " + count);
    if (seat === 0) {
        alert("No more seats available");
    }
}

return(
                <Card className="m-3">
                    <Card.Body>
                        <Card.Title>
                            { name }
                        </Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{ description }</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>Php { price }</Card.Text>
                        <Card.Text>Enrolees: {count}</Card.Text>
                        <Card.Text>Seats: {seat}</Card.Text>
                        <Button variant="primary" onClick={enroll}>Enroll</Button>
                    </Card.Body>
                </Card>
    )
};

CourseCard.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired, 
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}