const CourseData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! ",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! ",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! ",
        price: 45000,
        onOffer: true
    },
]
export default CourseData;